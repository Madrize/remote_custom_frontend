import React from 'react';
import ReactDOM from 'react-dom';

import Link from 'next/link'
import Router from 'next/router'

import Head from '../components/head'
import Nav from '../components/nav'
import Layout from '../components/layout'

import JobPreview from '../components/jobs/job_preview.js'

import apiSettings from '../components/config/settings.js'

import WithAuth from '../components/auth/with_auth.js'

import axios from 'axios'


class Search extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      query: null,
      skills: [],
      category: null,
      ordering: null,
      queryDict: {}
    }
  }

  addSkillFilter = (e) => {
    const exSkills = this.state.skills
    const newSkill = e.target.name
    if (exSkills.indexOf(newSkill) < 0){
      exSkills.push(newSkill)
      this.setState({
        skills: exSkills
      })
      this.filtersUpdated(exSkills, this.state.category)
      e.preventDefault()
    }
  }

  removeSkillFilter = (e) => {
    const i = this.state.skills.indexOf(e.target.name)
    const exSkills = this.state.skills
    exSkills.splice(i, 1)
    this.setState({
      skills: exSkills
    })
    this.filtersUpdated(exSkills, this.state.category)
  }

  toggleCategoryFilter = (e) => {

    const currentVal = this.state.category
    const nextVal = e.target.name

    let updated = false

    if (currentVal == nextVal){
      this.setState({
        category: null
      })

    } else if (currentVal == null){
      this.setState({
        category: nextVal
      })
      this.filtersUpdated(this.state.skills, nextVal)

    } else {
      this.setState({
        category: nextVal
      })
      this.filtersUpdated(this.state.skills, nextVal)
    }
  }

  buildQuery(skills, category){
    let filters = []
    if (skills.length > 0){
      filters.push("skills=" + skills.join(","))
    }
    if (category != null){
      filters.push("category=" + category)
    }
    let query = "?" + filters.join('&')
    return query
  }

  filtersUpdated(skills, category){

    const query = this.buildQuery(skills, category)
    
    this.reSearch(query)

    Router.push(
        '/search',
        '/search' + query,
        { shallow: true }
    )

  }

  async reSearch(query){

    const url = apiSettings.apiUrl + '/jobs/' + query

    const response = await axios({
      method: "get",
      url: url,
      auth: {
          username: apiSettings.apiUser,
          password: apiSettings.apiPassword
      }
    });

    this.setState({
      jobs: response.data.results,
    })

  }

  static async getInitialProps (context) {

    let skills = [];
    try {
      skills = context.query.skills.split(",").map((i) => i.trim())
    } catch (e){

    }

    let query = "?"
    if (skills.length > 0){
      query += "skills=" + skills.join(",")
    } else {
      query += "skills="
    }

    const url = apiSettings.apiUrl + '/jobs/' + query

    const response = await axios({
      method: "get",
      url: url,
      auth: {
          username: apiSettings.apiUser,
          password: apiSettings.apiPassword
      }
    });

    return {
      "jobs": response.data.results,
      "skills": skills,
      "context": context.query,
    }

  }

  componentDidMount(){
    this.setState({
      skills:this.props.skills,
      jobs:this.props.jobs
    })
  }

  componentWillReceiveProps(newProps){

  }

  render(){
    const title = this.state.skills ? this.state.skills.join(", ") + " jobs" : "remote jobs"
    return (
      <Layout title={title} user={this.props.user}>

        <section className="section section-hero section-search">
          <div className="container">

            <div className="columns">
              
              <div className="column is-12 formy">

                <h1 className="title">Game Dev Jobs</h1>
                <h2 className="subtitle">
                  Daily game development jobs all over the world
                </h2>
                
                <form>
                    
                  <div className="field has-addons">

                    <div className="control search-input">
                      <input className="input" type="text" placeholder="Find jobs" />
                    </div>

                    <div className="control">
                      <a className="button is-info search-button">
                        Search
                      </a>
                    </div>

                  </div>

                  <div className="tags">
                    <a className="button tag" name="programming" onClick={this.toggleCategoryFilter}>Programming</a>
                    <a className="button tag" name="design" onClick={this.toggleCategoryFilter}>Design</a>
                    <a className="button tag" name="copywriting" onClick={this.toggleCategoryFilter}>Copywriting</a>
                    <a className="button tag" name="devops" onClick={this.toggleCategoryFilter}>DevOps</a>
                    <a className="button tag" name="marketing" onClick={this.toggleCategoryFilter}>Marketing</a>
                  </div>

                </form>

                <div className="column applied-filters">

                  {this.state.skills 
                    && this.state.skills.map(function (job, index){
                      return (
                        <AppliedFilter title={job} key={index} onDelete={this.removeSkillFilter} />     
                      )
                  }, this)}

                </div>

              </div>

            </div>

          </div>
        </section>

        <section className="section section-main">
          <div className="container job-list">

            <div className="columns">

              <div className="column is-12">

                {this.state.jobs ? 
                  this.state.jobs.map(function (job, index) {
                    const skills = job.skills.map(function(s, i){ return s.title });
                    return(
                      <JobPreview companyTitle={job.company.title} title={job.title} skills={skills} 
                        timeSince={job.time_since} key={index} slug={job.slug} onClick={this.addSkillFilter} source="search" />
                    )
                  }, this):
                  <div>No jobs found.</div>
                }
                
              </div>

            </div>

          </div>
        </section>
        
      </Layout>
    )
  }

}



const AppliedFilter = (props) => {
  return (
    <span className="tags has-addons">
      <span className="tag is-danger">{props.title}</span>
      <a className="tag is-delete" onClick={props.onDelete} name={props.title}></a>
    </span>
  )
}


export default WithAuth(Search)
