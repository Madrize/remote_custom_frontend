import React from 'react';
import ReactDOM from 'react-dom';

import Link from 'next/link'

import Layout from '../components/layout'

import apiSettings from '../components/config/settings.js'

import axios from 'axios'


class About extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email:"",
      content:""
    };
  }

  setFieldValue = (e) => {
    const key = e.target.getAttribute("name")
    this.setState({
      [e.target.getAttribute("name")]:e.target.value
    })
  }

  submitForm = (e) => {
    console.log(this.state)
    this.sendData();
    e.preventDefault()
  }

  async sendData(){
    const response = await axios({
      method: "post",
      url: apiSettings.baseUrl + "/api/contact/",
      data: {
        "email": this.state.email,
        "content": this.state.content,
        "submit": true
      },
      auth: {
              username: apiSettings.apiUser,
              password: apiSettings.apiPassword
          }
    });
  }

  resetForm = (e) => {
    this.setState({
      email:"",
      content:""
    })
    e.preventDefault()
  }

  render(){
    return (
      <Layout title="about us">

        <section className="section section-hero">
          <div className="container">


          <div className="title">Contact Us</div>

          <div>

            <form>

            <div className="field">
              <label className="label">Email</label>
              <div className="control has-icons-left has-icons-right">
                <input className="input" type="email" placeholder="Email input" name="email" onChange={this.setFieldValue} />
                <span className="icon is-small is-left">
                  <i className="fas fa-envelope"></i>
                </span>
              </div>
            </div>

            <div className="field">
              <label className="label">Subject</label>
              <div className="control">
                <div className="select">
                  <select>
                    <option>-</option>
                    <option value="general">General Question</option>
                  </select>
                </div>
              </div>
            </div>

            <div className="field">
              <label className="label">Message</label>
              <div className="control">
                <textarea className="textarea" placeholder="Textarea" name="content" onChange={this.setFieldValue}></textarea>
              </div>
            </div>

            <div className="field is-grouped">
              <div className="control">
                <button className="button is-link" onClick={this.submitForm}>Submit</button>
              </div>
              <div className="control">
                <button className="button is-text" onClick={this.resetForm}>Cancel</button>
              </div>
            </div>

            </form>

          </div>

          </div>
        </section>

      </Layout>
    )
  }
}

export default About;