import React from 'react';
import ReactDOM from 'react-dom';

import Link from 'next/link'

import Head from '../components/head'
import Nav from '../components/nav'
import Layout from '../components/layout'
import apiSettings from '../components/config/settings.js'
import WithAuth from '../components/auth/with_auth.js'

import axios from 'axios'


class Job extends React.Component {

  constructor(props) {
    super(props);
  }

  static async getInitialProps (context) {

    const response = await axios({
      method: "get",
      url: apiSettings.apiUrl + '/jobs/' + context.query.slug,
      auth: {
        username: apiSettings.apiUser,
        password: apiSettings.apiPassword
      }
    });

    return {
      "job": response.data,
      "context": context.query
    }

  }

  render(){
    return (
      <Layout title="" user={this.props.user}>

        <section class="section section-hero">
          <div class="container">

            <div class="columns">
              
              <div class="column is-10">

                <h2 class="title">{this.props.job.title}</h2>

                <div class="job-details">
                  {this.props.job.job_type &&
                  <span class="detail-item">
                    <span class="icon">
                      <i class="fas fa-home"></i>
                    </span>Full Time
                  </span>
                  }
                  {this.props.job.location &&
                  <span class="detail-item">
                    <span class="icon">
                      <i class="fas fa-home"></i>
                    </span>Istanbul
                  </span>
                  }
                </div>

              </div>
              
              <div class="column is-2">

                <button class="button is-info">Apply for this job</button>

              </div>

            </div>

          </div>
        </section>

        <section class="section section-main">
          <div class="container job-detail">

            <div class="columns">
              
              <div class="column is-9">

                <div class="content">

                  {this.props.job.content}

                </div>

              </div>
              
              <div class="column is-3 sidebar">

                <div class="sidebar-item">
                  <img class="company-logo" src="https://we-work-remotely.imgix.net/logos/0006/6543/logo.gif?ixlib=rails-2.1.3&w=190&min-h=150" />
                </div>

                <div class="sidebar-item">
                  <form>
                    Subscribe to get notifications about similar jobs
                    <div class="field has-addons">
                      <div class="control">
                        <input class="input is-expanded" type="text" placeholder="Find a repository" />
                      </div>
                      <div class="control">
                        <a class="button is-info subscribe">
                          OK
                        </a>
                      </div>
                    </div>
                  </form>
                </div>

              </div>

            </div>

          </div>
        </section>
        
      </Layout>
    )
  }

}

export default WithAuth(Job)
