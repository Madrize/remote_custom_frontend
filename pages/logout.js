import React from 'react';
import ReactDOM from 'react-dom';

import Layout from '../components/layout'
import Modal from '../components/modal.js'
import Auth from '../components/auth/auth.js'


class Logout extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			isLoggedIn: false
		};
		this.auth = new Auth()
	}

	componentDidMount(){
		if (this.auth.isLoggedIn())
			this.auth.logout()
	}

	render(){
		return (
			<Layout>
				Redirecting...
			</Layout>
		)
	}

}

export default Logout

