import React from 'react';
import ReactDOM from 'react-dom';

import Link from 'next/link'

import Head from '../components/head'
import Nav from '../components/nav'
import Layout from '../components/layout'

import JobPreview from '../components/jobs/job_preview.js'

import apiSettings from '../components/config/settings.js'

import axios from 'axios'


class Index extends React.Component {

  constructor(props) {
    super(props);
  }

  static async getInitialProps (context) {

    const slug = context.query.slug

    const response = await axios({
      method: "get",
      url: apiSettings.apiUrl + '/jobs/',
      auth: {
          username: apiSettings.apiUser,
          password: apiSettings.apiPassword
      }
    });
    
    return {
      "jobs": response.data.results,
      "slug": slug
    }

  }

  render(){
    return (
      <Layout title={"remote " + this.props.slug + " jobs"}>

        <section className="section section-hero">
          <div className="container">

            <div className="columns">
              
              <div className="column is-12 formy">

                <h1 className="title">Game Dev Jobs</h1>
                <h2 className="subtitle">
                  Daily game development jobs all over the world
                </h2>
                
                <form>
                    
                  <div className="field has-addons">

                    <div className="control search-input">
                      <input className="input" type="text" placeholder="Find jobs" />
                    </div>

                    <div className="control">
                      <a className="button is-info search-button">
                        Search
                      </a>
                    </div>

                  </div>

                  <div className="tags">
                    <Link href="/search?category=programming">
                      <a className="button tag">Programming</a>
                    </Link>
                    <Link href="/search?category=design">
                      <a className="button tag">Design</a>
                    </Link>
                    <Link href="/search?category=copywriting">
                      <a className="button tag">Copywriting</a>
                    </Link>
                    <Link href="/search?category=devops">
                      <a className="button tag">DevOps</a>
                    </Link>
                    <Link href="/search?category=marketing">
                      <a className="button tag">Marketing</a>
                    </Link>
                  </div>

                </form>

              </div>

            </div>

          </div>
        </section>

        <section className="section section-main">
          <div className="container job-list">

            <div className="columns">

              <div className="column is-12">

                <div className="columns job-group-title">
                  Programming Jobs
                </div>

                {this.props.jobs ? 
                  this.props.jobs.map(function (job, index) {
                    const skills = job.skills.map(function(s, i){ return s.title });
                    return(
                      <JobPreview companyTitle={job.company.title} title={job.title} skills={skills} 
                        timeSince={job.time_since} key={index} slug={job.slug} source="search" />
                    )
                  }):
                  <div>No jobs found.</div>
                }
                
              </div>

            </div>

          </div>
        </section>
        
      </Layout>
    )
  }

}


export default Index
