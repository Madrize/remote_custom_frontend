import React from 'react';
import ReactDOM from 'react-dom';

import Link from 'next/link'

import Layout from '../components/layout'

import WithAuth from '../components/auth/with_auth.js'


class Account extends React.Component {

  static async getInitialProps (context) {

    return {
      "context": context.query,
    }

  }

  render(){
    return (
      <Layout title="about us" user={this.props.user}>

        <section className="section section-hero">
          <div className="container">

          	{this.props.user ? 
              <div>
                <p>Account details</p>
                <p><Link href="/logout">Logout</Link></p>
              </div>
              :
              <p>You dont have the permission to view this page</p>
            }

          </div>
        </section>

      </Layout>
    )
  }
}

export default WithAuth(Account);