import Link from 'next/link'

import Modal from '../components/modal.js'

import LoginForm from '../components/forms/login_form.js'
import RegistrationForm from '../components/forms/registration_form.js'


const NavItem = (props) => {
  return (
    <div className="custom-navbar-item">
      <Link href={props.href || "#"}>
        <a className="navbar-item" onClick={props.onClick}>
          <span className="icon">
            <i className={props.icon}></i>
          </span>{props.title}
        </a>
      </Link>
    </div>
  )
}


class Nav extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      accountModalActive: false,
      modalChoice: "signin",
      registrationSuccess: false
    };
  }

  onItemClick = (e) => {
    e.preventDefault()
    this.setState({accountModalActive:true})
  }

  onModalClose = (e) => {
    this.setState({accountModalActive:false})
  }

  onLogout = (e) => {
    (new Auth()).logout()
  }

  onLoginSelected = (e) => {
    if (this.state.modalChoice != "signin"){
      this.setState({
        modalChoice: "signin"
      })
      console.log(this.state.modalChoice)
    }
  }

  onRegistrationSuccess(){
      this.setState({
        modalChoice: "signin",
        registrationSuccess: true
      })
      console.log(this.state.modalChoice)
  }

  onRegistrationSelected = (e) => {
    if (this.state.modalChoice != "signup"){
      this.setState({
        modalChoice: "signup"
      })
      console.log(this.state.modalChoice)
    }
  }

  render(){
    return (
      <nav className="navbar is-light-blue">

        <div className="container">

          <div className="navbar-brand">
            <Link href="/">
              <a className="navbar-item">
                <img src="https://www.gams.com/typo3conf/ext/gamsdistribution/Resources/Public/images/facebook.png" width="112" height="28" />
              </a>
            </Link>
          </div>

          <div id="navbarExampleTransparentExample" className="navbar-menu">

            <div className="navbar-end">

              <NavItem title="Explore" icon="fas fa-home" />
              <NavItem title="Collab" icon="fas fa-users" />
              <NavItem title="Post a Job" icon="fas fa-plus" />

              {this.props.user ? 
                <NavItem title="Account" icon="fas fa-user" href="/account" />:
                <NavItem title="Sign In" icon="fas fa-user" onClick={this.onItemClick} />
              }

            </div>

          </div>

        </div>

        <Modal active={this.state.accountModalActive} onModalClose={this.onModalClose}>

          <form className="modal-form">

            {this.state.modalChoice == "signin" ?
            <LoginForm />:
            <RegistrationForm />}

            <div className="auth-helpers">
              <span className="frst">
                <a onClick={this.onLoginSelected}>Sign In</a>
              </span>
              <span className="scnd">
                <a onClick={this.onRegistrationSelected}>Create Account</a>
              </span>
              <span className="scnd">
                <a>Forgot Password</a>
              </span>
            </div>

          </form>

        </Modal>

      </nav>
    )
  }

}



export default Nav
