import React from 'react';
import ReactDOM from 'react-dom';

import Link from 'next/link'


class JobPreview extends React.Component {

  render(){
    return (
      <div className="columns job-item">

        <div className="column is-8">

          <div className="job-detail-item job-role">
            <Link as={`/job/${this.props.slug}`} href={`/job?slug=${this.props.slug}`}>
              <a>{this.props.title}</a>
            </Link>
          </div>

          <div className="job-role">
            <span className="job-hi">
              <span className="icon">
                <i className="fas fa-building"></i>
              </span>{this.props.companyTitle}
            </span>
            <span className="job-hi">
              <span className="icon">
                <i className="fas fa-map-marker-alt"></i>
              </span>İstanbul
            </span>
          </div>

          <div className="job-skills">
            <div className="tags">
              {this.props.skills && 
                this.props.skills.map(function (skill, index) {

                  if (!this.props.onClick){

                    return(
                      <Link href={"/search?skills=" + skill} key={index}>
                        <a className="tag">{skill}</a>
                      </Link>
                    )

                  } else {

                    return(
                      <a className="tag" name={skill} onClick={this.props.onClick}>{skill}</a>
                    )

                  }
                  
                }, this)
              }
            </div>
          </div>

        </div>

        <div className="column is-4">

          <div className="job-detail-item job-role job-date has-text-right">
            <span>{this.props.timeSince} days ago</span>
          </div>

        </div>

      </div>
    )
  }

}

export default JobPreview;