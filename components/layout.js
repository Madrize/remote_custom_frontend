import React from 'react';
import ReactDOM from 'react-dom';

import Footer from '../components/footer.js'
import Head from '../components/head.js'
import Nav from '../components/nav.js'


class Layout extends React.Component {

	render(){
		return (
			<div>
				<Head title={this.props.title} />
				<Nav user={this.props.user} />
				{this.props.children}
				<Footer />
			</div>
		)
	}

}

export default Layout;

