import React from 'react';
import ReactDOM from 'react-dom';

import Link from 'next/link'


const FooterLink = (props) => {
	return (
		<li><Link href={props.url || "#"}>{props.title}</Link></li>
	)
}


class Footer extends React.Component {

	render(){
		return (
			<footer className="footered">
				<div className="container">
					<div className="columns footered-item">
						<div className="column">
							<div className="title is-size-6">
								Game Development Jobs
							</div>
							<ul>
								<li><a href="">3D Modeler Jobs</a></li>
								<li><a href="">Sprite Artist Jobs</a></li>
								<li><a href="">Digital Artist Jobs</a></li>
							</ul>
						</div>
						<div className="column">
							<div className="title is-size-6">
								Game Development Jobs
							</div>
							<ul>
								<li><a href="">Unity Developer Jobs</a></li>
								<li><a href="">Unreal Engine Developer Jobs</a></li>
								<li><a href="">C# Developer Jobs</a></li>
								<li><a href="">Unity Developer Jobs</a></li>
								<li><a href="">Unity Developer Jobs</a></li>
							</ul>
						</div>
						<div className="column">
							<div className="title is-size-6">
							GamedevJobs
							</div>
							<ul>
								<li>
								  <Link href="/about">
									<a>About</a>
								  </Link>
								</li>
								<li>
								  <Link href="/contact">
									<a>Contact</a>
								  </Link>
								</li>
							</ul>
						</div>
					</div>
				    <div className="content footered-item">
				    	<div className="copyright">All rights reserved GameDevJobs ©2018</div>
				    </div>
				</div>
			</footer>
		)
	}

}


export default Footer;

