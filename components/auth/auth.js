import React from 'react';
import ReactDOM from 'react-dom';

import decode from 'jwt-decode';

import axios from 'axios'


class Auth {

    constructor(domain) {
        this.domain = domain || 'http://localhost:8000'
        this.fetch = this.fetch.bind(this)
        this.login = this.login.bind(this)
        this.getProfile = this.getProfile.bind(this)
    }

    login(username, password) {
    	const data = JSON.stringify({
                username,
                password
            })
        return this.fetch(`${this.domain}/rest-auth/login/`, {
            method: 'POST',
            body: data
        }).then(res => {
            this.setToken(res.token)
            console.log('logged in ' + res.token)
            return Promise.resolve(res);
        }).then(err => {
            console.log(err)
        })
    }

    isLoggedIn() {
        const token = this.getToken()
        console.log(!!token)
        return !!token// && !this.isTokenExpired(token)
    }

    isTokenExpired(token) {
        try {
            const decoded = decode(token);
            if (decoded.exp < Date.now() / 1000) {
                return true;
            }
            else
                return false;
        }
        catch (err) {
            return false;
        }
    }

    setToken(idToken) {
        localStorage.setItem('id_token', idToken)
    }

    getToken() {
        return localStorage.getItem('id_token')
    }

    logout() {
        localStorage.removeItem('id_token')
        window.location.href = "/"
    }

    getProfile() {
        return decode(this.getToken());
    }

    fetch(url, options) {

        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

        if (this.isLoggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken()
        }

        return fetch(url, {
            headers,
            ...options
        })
        .then(this._checkStatus)
        .then(response => response.json())
    }

    _checkStatus(response) {
        if (response.status >= 200 && response.status < 300) {
            return response
        } else {
            var error = new Error(response.statusText)
            error.response = response
            throw error
        }
    }
}

export default Auth