import React, { Component } from 'react';
import Auth from '../auth/auth.js'


const WithAuth = (AuthComponent) => {

    const auth = new Auth('http://localhost:8000')

    return class AuthWrapped extends Component {

        constructor() {
            super();
            this.state = {
                user: null
            }
        }

        static async getInitialProps(ctx) {
            return AuthComponent.getInitialProps(ctx)
        }

        componentDidMount() {

            if (!auth.isLoggedIn()) {
                //this.props.history.replace('/login')
            }

            else {
                try {
                    const profile = auth.getProfile()
                    this.setState({
                        user: profile
                    })
                }
                catch(err){
                    auth.logout()
                    //this.props.history.replace('/login')
                }
            }

        }

        render() {
            if (this.state.user) {
                    return (
                    <AuthComponent history={this.props.history} user={this.state.user} {...this.props} />
                )
            } else {
                return <AuthComponent {...this.props} />
            }

        }

    }

}


export default WithAuth
