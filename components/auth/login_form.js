import React from 'react';
import ReactDOM from 'react-dom';

import Link from 'next/link'

import apiSettings from '../settings.js'

import Auth from '../auth/auth.js'


class LoginForm extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			"email": "",
			"password": "",
			"error": null
		}
		this.auth = new Auth(apiSettings.baseUrl)
	}

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	submitForm = (e) => {

		e.preventDefault()

		this.auth.login(
			this.state.email, 
			this.state.password
		).then(res =>{
           window.location.href = "/"
        })
        .catch(err =>{
            this.setState({
            	"error": "Incorrect credentials"
            })
        })

	}

	render(){
		return (

				<div>

					<div className="modal-form-title">

						<h2>SIGN IN</h2>

					</div>

					<div className="modal-form-data">

						{this.state.error && <div className="form-error">Incorrect username or password</div>}

						<div className="field">
			                
							<div className="control">
								<input type="email" name="email" className="input" onChange={this.handleChange} placeholder="E-Mail" />
							</div>
						
						</div>

						<div className="field">
			                
							<div className="control">
								<input type="password" name="password" className="input" onChange={this.handleChange} placeholder="Password" />
							</div>
						
						</div>

						<div>
							<button className="button is-info" onClick={this.submitForm}>Sign In</button>
						</div>

		            </div>

				</div>
		)
	}

}




export default LoginForm

