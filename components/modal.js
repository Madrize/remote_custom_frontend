import React from 'react';
import ReactDOM from 'react-dom';


class Modal extends React.Component {

	render(){
		return (
			<div className={this.props.active ? "modal is-active":"modal"}>
			  <div className="modal-background"></div>
			  <div className="modal-content">
			    { this.props.children }
			  </div>
			  <button className="modal-close is-large" aria-label="close" onClick={this.props.onModalClose}></button>
			</div>
		)
	}

}

export default Modal

