import React from 'react';
import ReactDOM from 'react-dom';

import Link from 'next/link'

import apiSettings from '../config/settings.js'

import axios from 'axios'

import Auth from '../auth/auth.js'


class RegistrationForm extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			"username": "",
			"email": "",
			"password1": "",
			"password2": "",
			"error": null,
			"success": ""
		}
		this.auth = new Auth(apiSettings.baseUrl)
	}

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	submitForm = (e) => {

		e.preventDefault()

		const data = {
			"username": this.state.username,
			"email": this.state.email,
			"password1": this.state.password1,
			"password2": this.state.password2
		}

		console.log(data)

		axios({
			method: "post",
			url: apiSettings.baseUrl + '/rest-auth/registration/',
			data: data,
		}).then(result => 
			this.setState({
				success: true,
				error: null
			})
		).catch(error => 
  		  	this.setState({
				error: true,
				success: null
			})
		)

	}

	render(){
		return (

				<div>

					<div className="modal-form-title">

						<h2>REGISTRATION</h2>

					</div>

					<div className="modal-form-data">

						{this.state.error && <div className="form-error">Please check all the fields.</div>}

						{this.state.success && <div className="form-success">Your account is created. Please login.</div>}

						<div className="field">
			                
							<div className="control">
								<input type="text" name="username" className="input" onChange={this.handleChange} placeholder="Username" />
							</div>
						
						</div>

						<div className="field">
			                
							<div className="control">
								<input type="email" name="email" className="input" onChange={this.handleChange} placeholder="E-Mail" />
							</div>
						
						</div>

						<div className="field">
			                
							<div className="control">
								<input type="password" name="password1" className="input" onChange={this.handleChange} placeholder="Password" />
							</div>
						
						</div>

						<div className="field">
			                
							<div className="control">
								<input type="password" name="password2" className="input" onChange={this.handleChange} placeholder="Password (Again)" />
							</div>
						
						</div>

						<div>
							<button className="button is-info" onClick={this.submitForm}>Sign Up</button>
						</div>

		            </div>

			  </div>
		)
	}

}




export default RegistrationForm

